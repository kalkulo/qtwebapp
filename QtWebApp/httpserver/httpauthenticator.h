/**
  @file
  @author H�kon Enger
*/

#ifndef HTTPAUTHENTICATOR_H
#define HTTPAUTHENTICATOR_H

#include "httpglobal.h"
#include "httprequest.h"
#include "httpresponse.h"

namespace stefanfrings {

/**
   The authenticator controls authentication for each HTTP session.
   <p>
   You need to override the service() method or you will always get an HTTP error 401.
   <p>
   @warning Be aware that the main request handler instance must be created on the heap and
   that it is used by multiple threads simultaneously.
   @see StaticFileController which delivers static local files.
*/

class DECLSPEC HttpAuthenticator : public QObject {
    Q_OBJECT
    Q_DISABLE_COPY(HttpAuthenticator)
public:

    /**
     * Constructor.
     * @param parent Parent object.
     */
    HttpAuthenticator(QObject* parent=nullptr);

    /** Destructor */
    virtual ~HttpAuthenticator();

    /**
      Generate a response for an incoming HTTP request.
      @param request The received HTTP request
      @param response Must be used to return the response
      @warning This method must be thread safe
    */
    virtual void service(HttpRequest& request, HttpResponse& response);

    /**
     Returns true if the session is authenticated.
    */
    virtual bool authenticated() const;

    /**
    Returns the username if the session is authenticated.
    */
    virtual QString username() const;
};

typedef HttpAuthenticator* (*HttpAuthenticatorFactory)();

} // end of namespace

#endif // HTTPAUTHENTICATOR_H
