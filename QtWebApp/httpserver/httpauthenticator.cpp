/**
  @file
  @author H�kon Enger
*/

#include "httpauthenticator.h"

using namespace stefanfrings;

HttpAuthenticator::HttpAuthenticator(QObject* parent)
    : QObject(parent)
{}

HttpAuthenticator::~HttpAuthenticator()
{}

void HttpAuthenticator::service(HttpRequest& request, HttpResponse& response)
{
    qCritical("HttpAuthenticator: you need to override the service() function");
    qDebug("HttpAuthenticator: request=%s %s %s",request.getMethod().data(),request.getPath().data(),request.getVersion().data());
    response.setStatus(401,"Unauthorized");
    response.write("401 Unauthorized",true);
}

bool HttpAuthenticator::authenticated() const
{
    return false;
}

QString HttpAuthenticator::username() const
{
    return QString();
}
