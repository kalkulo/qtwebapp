/**
  @file
  @author Stefan Frings
*/

#include "httplistener.h"
#include "httpconnectionhandler.h"
#include "httpconnectionhandlerpool.h"
#include <QCoreApplication>

using namespace stefanfrings;

HttpListener::HttpListener(HttpRequestHandler* requestHandler, QObject *parent)
    : QTcpServer(parent), port(80), sslConfiguration(0), accessLog(0)
{
    Q_ASSERT(requestHandler!=nullptr);
    pool=nullptr;
    this->requestHandler=requestHandler;
    // Reqister type of socketDescriptor for signal/slot handling
    qRegisterMetaType<tSocketDescriptor>("tSocketDescriptor");
}


HttpListener::~HttpListener()
{
    close();
    delete sslConfiguration;
    qDebug("HttpListener: destroyed");
}


void HttpListener::setHost(const QString& host)
{
    this->host = host;
}


void HttpListener::setPort(int port)
{
    this->port = port & 0xFFFF;
}


void HttpListener::setSslConfig(const QSslConfiguration* config)
{
    if (sslConfiguration) delete sslConfiguration;

    if (config == nullptr) {
        sslConfiguration = nullptr;
    } else {
        sslConfiguration = new QSslConfiguration(*config);
    }

    if (pool) pool->setSslConfig(sslConfiguration);
}


void HttpListener::setAuthFactory(HttpAuthenticatorFactory authFactory)
{
    this->authFactory = authFactory;

    if (pool) pool->setAuthFactory(authFactory);
}


void HttpListener::setAccessLogFile(QIODevice* accessLog)
{
    this->accessLog = accessLog;

    if (pool) pool->setAccessLogFile(accessLog);
}


void HttpListener::listen()
{
    if (!pool)
    {
        pool=new HttpConnectionHandlerPool(requestHandler);
        pool->setSslConfig(sslConfiguration);
        pool->setAuthFactory(authFactory);
        pool->setAccessLogFile(accessLog);
    }
    QTcpServer::listen(host.isEmpty() ? QHostAddress::Any : QHostAddress(host), port);
    if (!isListening())
    {
        qCritical("HttpListener: Cannot bind on port %i: %s",port,qPrintable(errorString()));
    }
    else {
        qDebug("HttpListener: Listening on port %i",port);
    }
}


void HttpListener::close() {
    QTcpServer::close();
    qDebug("HttpListener: closed");
    if (pool) {
        delete pool;
        pool=nullptr;
    }
}

void HttpListener::incomingConnection(tSocketDescriptor socketDescriptor) {
#ifdef SUPERVERBOSE
    qDebug("HttpListener: New connection");
#endif

    HttpConnectionHandler* freeHandler=nullptr;
    if (pool)
    {
        freeHandler=pool->getConnectionHandler();
    }

    // Let the handler process the new connection.
    if (freeHandler)
    {
        // The descriptor is passed via event queue because the handler lives in another thread
        QMetaObject::invokeMethod(freeHandler, "handleConnection", Qt::QueuedConnection, Q_ARG(tSocketDescriptor, socketDescriptor));
    }
    else
    {
        // Reject the connection
        qDebug("HttpListener: Too many incoming connections");
        QTcpSocket* socket=new QTcpSocket(this);
        socket->setSocketDescriptor(socketDescriptor);
        connect(socket, SIGNAL(disconnected()), socket, SLOT(deleteLater()));
        socket->write("HTTP/1.1 503 too many connections\r\nConnection: close\r\n\r\nToo many connections\r\n");
        socket->disconnectFromHost();
    }
}
