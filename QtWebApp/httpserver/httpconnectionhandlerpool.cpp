#ifndef QT_NO_OPENSSL
    #include <QSslSocket>
    #include <QSslKey>
    #include <QSslCertificate>
    #include <QSslConfiguration>
#endif
#include <QDir>
#include "httpconnectionhandlerpool.h"

using namespace stefanfrings;

HttpConnectionHandlerPool::HttpConnectionHandlerPool(HttpRequestHandler *requestHandler)
    : QObject()
{
    this->requestHandler=requestHandler;
    this->sslConfiguration=NULL;
    this->authFactory=nullptr;
    this->accessLog=nullptr;
    this->cleanupInterval=1000;
    this->maxConnectionHandlers=100;
    this->maxIdleHandlers=1;

    cleanupTimer.start(cleanupInterval);
    connect(&cleanupTimer, SIGNAL(timeout()), SLOT(cleanup()));
}


HttpConnectionHandlerPool::~HttpConnectionHandlerPool()
{
    // delete all connection handlers and wait until their threads are closed
    foreach(HttpConnectionHandler* handler, pool)
    {
       delete handler;
    }
    delete sslConfiguration;
    qDebug("HttpConnectionHandlerPool (%p): destroyed", this);
}


HttpConnectionHandler* HttpConnectionHandlerPool::getConnectionHandler()
{
    HttpConnectionHandler* freeHandler=0;
    mutex.lock();
    // find a free handler in pool
    foreach(HttpConnectionHandler* handler, pool)
    {
        if (!handler->isBusy())
        {
            freeHandler=handler;
            freeHandler->setBusy();
            break;
        }
    }
    // create a new handler, if necessary
    if (!freeHandler)
    {
        if (pool.count()<maxConnectionHandlers)
        {
            freeHandler=new HttpConnectionHandler(requestHandler,sslConfiguration,authFactory,accessLog);
            freeHandler->setBusy();
            pool.append(freeHandler);
        }
    }
    mutex.unlock();
    return freeHandler;
}


void HttpConnectionHandlerPool::cleanup()
{
    int idleCounter=0;
    mutex.lock();
    foreach(HttpConnectionHandler* handler, pool)
    {
        if (!handler->isBusy())
        {
            if (++idleCounter > maxIdleHandlers)
            {
                delete handler;
                pool.removeOne(handler);
                qDebug("HttpConnectionHandlerPool: Removed connection handler (%p), pool size is now %i",handler,pool.size());
                break; // remove only one handler in each interval
            }
        }
    }
    mutex.unlock();
}


void HttpConnectionHandlerPool::setSslConfig(const QSslConfiguration* config)
{
    if (sslConfiguration) delete sslConfiguration;

    if (config == nullptr) {
        sslConfiguration = nullptr;
    } else {
        sslConfiguration = new QSslConfiguration(*config);
    }
}


void HttpConnectionHandlerPool::setAuthFactory(HttpAuthenticatorFactory authFactory)
{
    this->authFactory = authFactory;
}


void HttpConnectionHandlerPool::setAccessLogFile(QIODevice* accessLog)
{
    this->accessLog = accessLog;
}


void HttpConnectionHandlerPool::setCleanupInterval(int cleanupInterval)
{
    this->cleanupInterval = cleanupInterval;
}


void HttpConnectionHandlerPool::setMaxThreads(int maxThreads)
{
    this->maxConnectionHandlers = maxThreads;
}


void HttpConnectionHandlerPool::setMinThreads(int minThreads)
{
    this->maxIdleHandlers = minThreads;
}