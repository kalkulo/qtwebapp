/**
  @file
  @author Stefan Frings
*/

#ifndef HTTPLISTENER_H
#define HTTPLISTENER_H

#include <QTcpServer>
#include <QBasicTimer>
#include <QIODevice>
#include "httpglobal.h"
#include "httpconnectionhandler.h"
#include "httpconnectionhandlerpool.h"
#include "httprequesthandler.h"

namespace stefanfrings {

/**
  Listens for incoming TCP connections and and passes all incoming HTTP requests to your implementation of HttpRequestHandler,
  which processes the request and generates the response (usually a HTML document).
  <p>
  The optional host parameter binds the listener to one network interface.
  The listener handles all network interfaces if no host is configured.
  The port number specifies the incoming TCP port that this listener listens to.
  @see HttpConnectionHandlerPool for description of config settings minThreads, maxThreads, cleanupInterval and ssl settings
  @see HttpConnectionHandler for description of the readTimeout
  @see HttpRequest for description of config settings maxRequestSize and maxMultiPartSize
*/

class DECLSPEC HttpListener : public QTcpServer {
    Q_OBJECT
    Q_DISABLE_COPY(HttpListener)
public:

    /**
      Constructor.
      Creates a connection pool and starts listening on the configured host and port.
      @param settings Configuration settings, usually stored in an INI file. Must not be 0.
      Settings are read from the current group, so the caller must have called settings->beginGroup().
      Because the group must not change during runtime, it is recommended to provide a
      separate QSettings instance that is not used by other parts of the program.
      The HttpListener does not take over ownership of the QSettings instance, so the
      caller should destroy it during shutdown.
      @param requestHandler Processes each received HTTP request, usually by dispatching to controller classes.
      @param parent Parent object.
      @warning Ensure to close or delete the listener before deleting the request handler.
    */
    HttpListener(HttpRequestHandler* requestHandler, QObject* parent=nullptr);

    /** Destructor */
    virtual ~HttpListener();

    /**
      Set hostname.
    */
    void setHost(const QString& host);

    /**
      Set port.
    */
    void setPort(int port);

    /**
     Set SSL Configuration.
    */
    void setSslConfig(const QSslConfiguration* sslConfiguration);


    /**
     Set authenticator factory 
    */
    void setAuthFactory(HttpAuthenticatorFactory authFactory);

    /**
    Set log file
    */
    void setAccessLogFile(QIODevice* logFile);

    /**
      Start listening.
    */
    void listen();

    /**
     Closes the listener, waits until all pending requests are processed,
     then closes the connection pool.
    */
    void close();

protected:

    /** Serves new incoming connection requests */
    void incomingConnection(tSocketDescriptor socketDescriptor);

private:

    /** Hostname */
    QString host;

    /** Port */
    quint16 port;

    /** The SSL configuration (certificate, key and other settings) */
    QSslConfiguration* sslConfiguration;

    /** Authenticator factory */
    HttpAuthenticatorFactory authFactory;
    
    /** Access log file */
    QIODevice* accessLog;

    /** Point to the reuqest handler which processes all HTTP requests */
    HttpRequestHandler* requestHandler;

    /** Pool of connection handlers */
    HttpConnectionHandlerPool* pool;

signals:

    /**
      Sent to the connection handler to process a new incoming connection.
      @param socketDescriptor references the accepted connection.
    */

    void handleConnection(tSocketDescriptor socketDescriptor);

};

} // end of namespace

#endif // HTTPLISTENER_H
